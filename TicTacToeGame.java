import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		boolean stopGame = false;
		int player1Wins = 0;
		int player2Wins = 0;
		
		while(!(stopGame)){
			System.out.println("Let's play Tic-Tac-Toe!");
			System.out.println("Player 1's token: " + Square.X);
			System.out.println("Player 2's token: " + Square.O);
			Board board = new Board();
			boolean gameOver = false;
			int player = 1;
			Square playerToken = Square.X;
			
			while(!(gameOver)){
				
				if(player == 1){
					playerToken = Square.X;
				}else{
					playerToken = Square.O;
				}
				
				System.out.println("Player " + player + ": It's your turn! Where do you want to place your token?");
				int row = reader.nextInt();
				int column = reader.nextInt();
				
				while(!(board.placeToken(row, column, playerToken))){
					System.out.println("Please re-enter correct row and column values:");
					row = reader.nextInt();
					column = reader.nextInt();
				}
				
				board.placeToken(row, column, playerToken);
				System.out.println(board);
				
				if(board.checkIfWinning(playerToken)){
					System.out.println("Player " + player + " wins!");
					if(player == 1){
						player1Wins++;
					}else{
						player2Wins++;
					}
					gameOver = true;
				}else if(board.checkIfFull()){
					System.out.println("It's a tie!");
					gameOver = true;
				}else{
					player++;
					if(player > 2){
						player = 1;
					}
				}
			}
			System.out.println("Want to play another game? Enter [0] if No, any number if Yes");
			int userInput = reader.nextInt();
			if(userInput == 0){
				stopGame = true;
			}
		}
		System.out.println("Player 1 won " + player1Wins + " game(s).");
		System.out.println("Player 2 won " + player2Wins + " game(s).");
		System.out.println("Thanks for playing Tic-Tac-Toe!");
	}
}