public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	public String toString(){
		String boardSquares = "  1 2 3\n";
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			boardSquares += i+1 + " ";
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				boardSquares += this.tictactoeBoard[i][j] + " ";
			}
			boardSquares += "\n";
		}
		return boardSquares;
	}
	public boolean placeToken(int row, int col, Square playerToken){
		if(!((row >= 1 && row <= 3)&&(col >= 1 && col <= 3))){
			return false;
		}else if(this.tictactoeBoard[row-1][col-1] == Square.BLANK){
			this.tictactoeBoard[row-1][col-1] = playerToken;
			return true;
		}else{
			return false;
		}
	}
	public boolean checkIfFull(){
		int blankCount = 0;
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				if(this.tictactoeBoard[i][j] == Square.BLANK){
					blankCount++;
				}
			}
		}
		if(blankCount == 0){
			return true;
		}else{
			return false;
		}
	}
	private boolean checkIfWinningHorizontal(Square playerToken){
		int horizontalWinCount = 0;
		boolean hasWon = false;
			for(int i = 0; i < this.tictactoeBoard.length; i++){
				for(int j = 0; j < this.tictactoeBoard.length; j++){
					if(this.tictactoeBoard[i][j] == playerToken){
						horizontalWinCount++;
					}
				}
				if(horizontalWinCount == 3 && !(hasWon)){
					hasWon = true;
				}
				horizontalWinCount = 0;
			}
		return hasWon;
	}
	private boolean checkIfWinningVertical(Square playerToken){
		int verticalWinCount = 0;
		boolean hasWon = false;
			for(int j = 0; j < this.tictactoeBoard.length; j++){
				for(int i = 0; i < this.tictactoeBoard.length; i++){
					if(this.tictactoeBoard[i][j] == playerToken){
						verticalWinCount++;
					}
				}
				if(verticalWinCount == 3 && !(hasWon)){
					hasWon = true;
				}
				verticalWinCount = 0;
			}
		return hasWon;
	}
	private boolean checkIfWinningDiagonal(Square playerToken){
		if(this.tictactoeBoard[1][1] == playerToken && ((this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[2][2] == playerToken) || (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[2][0] == playerToken))){
			return true;
		}else{
			return false;
		}
	}
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}else{
			return false;
		}
	}
}